#pragma once
#include <time.h>

void time_init_timer();
double glfwGetTime();
void glfwSetTime(double);