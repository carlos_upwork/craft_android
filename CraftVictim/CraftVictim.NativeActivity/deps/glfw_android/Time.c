#include "glfw_android/time.h"


static double app_start_time = 0;
void time_init_timer()
{
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);
	app_start_time = (double)now.tv_nsec;
}

static double getTimeSinceAppStart()
{
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);
	return ((double)now.tv_nsec) - app_start_time;
}

double glfwGetTime()
{	//todo make sure this return time since app start something like 200 ked fps update
	return getTimeSinceAppStart();
}

void glfwSetTime(double time)
{
	app_start_time = time;
}



